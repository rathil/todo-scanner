package internal

import (
	"bytes"
	"fmt"
	"go/parser"
	"go/token"
	"os"
	"strings"
)

func FindToDo(
	baseDir string,
	dir string,
	todoList *[]Todo,
) error {
	pkgs, err := parser.ParseDir(token.NewFileSet(), dir, nil, parser.ParseComments)
	if err != nil {
		return fmt.Errorf(`parse dir [%s], err: %w`, dir, err)
	}
	for _, pkg := range pkgs {
		for name, file := range pkg.Files {
			var fileData []byte
			for _, comment := range file.Comments {
				if !strings.Contains(strings.ToLower(comment.Text()), `todo`) {
					continue
				}
				if len(fileData) == 0 {
					fileData, err = os.ReadFile(name)
					if err != nil {
						return fmt.Errorf(`read file [%s], err: %w`, name, err)
					}
				}
				comments := make([]string, 0, len(comment.List))
				for _, c := range comment.List {
					comments = append(comments, c.Text)
				}
				*todoList = append(*todoList, Todo{
					Line: bytes.Count(fileData[:comment.Pos()-file.Pos()], []byte("\n")) + 1,
					Path: strings.TrimPrefix(name, baseDir),
					Text: strings.Join(comments, "\n"),
				})
			}
		}
	}
	return nil
}
