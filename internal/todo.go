package internal

type Todo struct {
	Line int
	Path string
	Text string
}
