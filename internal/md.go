package internal

import (
	"fmt"
	"sort"
)

func CreateMd(
	name string,
	uri string,
	todoList []Todo,
) string {
	result := fmt.Sprintf("## %s TODO\n\n", name)
	sort.SliceStable(todoList, func(i, j int) bool {
		return todoList[i].Path < todoList[j].Path
	})
	for _, td := range todoList {
		result += fmt.Sprintf("### [%[1]s:%[2]d](%[3]s%[1]s#L%[2]d)\n\n", td.Path, td.Line, uri)
		result += fmt.Sprintf("```go\n%s\n```\n\n", td.Text)
	}
	return result
}
