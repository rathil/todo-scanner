FROM golang:1.18-alpine AS builder
WORKDIR /source/
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -installsuffix "static" -o /app cmd/todo-scanner/main.go

# image

FROM alpine:3.14
WORKDIR /source
RUN apk add --no-cache ca-certificates git
COPY --from=builder /app /app

ENV GIT_NAME="todo-scanner"
ENV GIT_EMAIL="rathil@rathil.com"
ENV GIT_REPO=""
ENV TITLE=""
ENV URI=""
ENV OUT_PATH=""
ENV CI_COMMIT_MESSAGE="TODO scanner result"

CMD git config --global user.name "${GIT_NAME}" \
    && git config --global user.email "${GIT_EMAIL}" \
    && git clone ${GIT_REPO} /result \
    && /app -t "${TITLE}" -o /result/${OUT_PATH} -uri ${URI} -p . \
    && cd /result \
    && git add . \
    && git commit -m "${CI_COMMIT_MESSAGE}" \
    && git push -f -u origin $(git branch --show-current)