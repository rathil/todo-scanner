package main

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/todo-scanner/internal"
	"io/ioutil"
	"log"
	"os"
	"path"
)

func main() {
	if err := (&cli.App{
		Name:  "TODO scanner",
		Usage: `Scanning GO code for TODOs in comments`,
		Flags: []cli.Flag{
			&cli.PathFlag{
				Name:     `path`,
				Usage:    `Path where to start scanning`,
				Aliases:  []string{`p`},
				EnvVars:  []string{`SCANNING_FOLDER`},
				Required: true,
			},
			&cli.PathFlag{
				Name:     `out`,
				Usage:    `Path to the file to save the result`,
				Aliases:  []string{`o`},
				EnvVars:  []string{`OUT_PATH`},
				Required: true,
			},
			&cli.StringFlag{
				Name:     `title`,
				Usage:    `Title of project`,
				Aliases:  []string{`t`},
				EnvVars:  []string{`TITLE`},
				Required: true,
			},
			&cli.StringFlag{
				Name:    `uri`,
				Usage:   `URI prefix`,
				EnvVars: []string{`URI_PREFIX`},
			},
		},
		Action: func(ctx *cli.Context) error {
			baseDir := path.Clean(ctx.Path(`path`)) + `/`
			dirs := []string{baseDir}
			var todoList []internal.Todo
			for i := 0; i < len(dirs); i++ {
				dir := dirs[i]
				if err := internal.FindToDo(baseDir, dir, &todoList); err != nil {
					return err
				}
				files, err := ioutil.ReadDir(dir)
				if err != nil {
					return err
				}
				for _, file := range files {
					if file.IsDir() {
						dirs = append(dirs, path.Join(dir, file.Name()))
					}
				}
			}
			if err := os.MkdirAll(path.Dir(ctx.Path(`out`)), 0766); err != nil && !os.IsExist(err) {
				return err
			}
			return os.WriteFile(
				ctx.Path(`out`),
				[]byte(internal.CreateMd(
					ctx.Path(`title`),
					ctx.Path(`uri`),
					todoList,
				)),
				0766,
			)
		},
		EnableBashCompletion: true,
		Authors: []*cli.Author{
			{
				Name:  "Ievgenii Kyrychenko",
				Email: "rathil@rathil.com",
			},
		},
	}).Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
